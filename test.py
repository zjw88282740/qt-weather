#!/usr/bin/env python3
#coding: utf-8
import urllib2, urllib, json,sys
if(len(sys.argv)<2):
    url="http://ip-api.com/json"
    ip_info=urllib2.urlopen(url).read()
    ip_data=json.loads(ip_info)
    #print ip_data['city']
    pm2_5="https://free-api.heweather.com/s6/air/now?location="+ip_data['city']+"&key=8cc8d54bb7ca42b9958b2b68fe1948ee"
    yql_query = '''select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'''+ip_data['city']+'''") and u='c' '''
else:
    pm2_5="https://free-api.heweather.com/s6/air/now?location="+sys.argv[1]+"&key=8cc8d54bb7ca42b9958b2b68fe1948ee"
    yql_query = '''select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="'''+sys.argv[1]+'''") and u='c' '''

baseurl = "https://query.yahooapis.com/v1/public/yql?"

#print (pm2_5)
#yql_query = "select * from weather.forecast where woeid=2161842 and u='c'"
yql_url = baseurl + urllib.urlencode({'q':yql_query}) + "&format=json"
#print yql_url

pm2_5_result=urllib2.urlopen(pm2_5).read()
result = urllib2.urlopen(yql_url).read()
#print (result)
pm2_5_data=json.loads(pm2_5_result)
print (pm2_5_data['HeWeather6'][0]['air_now_city'])
data = json.loads(result)
#print (data['query']['results'])
if(data['query']['results']==None):
    with open("tmp.db","wb") as f:
        f.write("DEADBEEF\n")
    f.close()

else:
    forecast = data['query']['results']['channel']['item']['forecast']
    cond = data['query']['results']['channel']['item']['condition']
    with open("tmp.db","wb") as f:
        for fore in forecast:
            f.write(fore['day']+','+fore['high']+','+fore['low']+','+fore['code']+'\n')
        f.write(cond['temp']+','+cond['date']+','+cond['text']+','+data['query']['results']['channel']['location']['city']+'\n')
        if(pm2_5_data['HeWeather6'][0]["status"]=="unknown city"):
            f.write("BEEFBEEF\n")
        else:
            f.write(pm2_5_data['HeWeather6'][0]['air_now_city']['pm25']+','+pm2_5_data['HeWeather6'][0]['air_now_city']['pm10']+','+pm2_5_data['HeWeather6'][0]['air_now_city']['co']+','+pm2_5_data['HeWeather6'][0]['air_now_city']['aqi']+'\n')
    f.close()



'''
{
 "query": {
  "count": 1,
  "created": "2018-09-09T04:31:31Z",
  "lang": "zh-CN",
  "results": {
   "channel": {
    "units": {
     "distance": "mi",
     "pressure": "in",
     "speed": "mph",
     "temperature": "F"
    },
    "title": "Yahoo! Weather - Nome, AK, US",
    "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/",
    "description": "Yahoo! Weather for Nome, AK, US",
    "language": "en-us",
    "lastBuildDate": "Sat, 08 Sep 2018 08:31 PM AKDT",
    "ttl": "60",
    "location": {
     "city": "Nome",
     "country": "United States",
     "region": " AK"
    },
    "wind": {
     "chill": "46",
     "direction": "248",
     "speed": "11"
    },
    "atmosphere": {
     "humidity": "85",
     "pressure": "1024.0",
     "rising": "0",
     "visibility": "16.1"
    },
    "astronomy": {
     "sunrise": "8:4 am",
     "sunset": "9:53 pm"
    },
    "image": {
     "title": "Yahoo! Weather",
     "width": "142",
     "height": "18",
     "link": "http://weather.yahoo.com",
     "url": "http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif"
    },
    "item": {
     "title": "Conditions for Nome, AK, US at 07:00 PM AKDT",
     "lat": "64.499474",
     "long": "-165.405792",
     "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/",
     "pubDate": "Sat, 08 Sep 2018 07:00 PM AKDT",
     "condition": {
      "code": "28",
      "date": "Sat, 08 Sep 2018 07:00 PM AKDT",
      "temp": "49",
      "text": "Mostly Cloudy"
     },
     "forecast": [
      {
       "code": "30",
       "date": "08 Sep 2018",
       "day": "Sat",
       "high": "51",
       "low": "43",
       "text": "Partly Cloudy"
      },
      {
       "code": "28",
       "date": "09 Sep 2018",
       "day": "Sun",
       "high": "54",
       "low": "46",
       "text": "Mostly Cloudy"
      },
      {
       "code": "30",
       "date": "10 Sep 2018",
       "day": "Mon",
       "high": "55",
       "low": "46",
       "text": "Partly Cloudy"
      },
      {
       "code": "26",
       "date": "11 Sep 2018",
       "day": "Tue",
       "high": "53",
       "low": "50",
       "text": "Cloudy"
      },
      {
       "code": "28",
       "date": "12 Sep 2018",
       "day": "Wed",
       "high": "51",
       "low": "47",
       "text": "Mostly Cloudy"
      },
      {
       "code": "39",
       "date": "13 Sep 2018",
       "day": "Thu",
       "high": "53",
       "low": "47",
       "text": "Scattered Showers"
      },
      {
       "code": "39",
       "date": "14 Sep 2018",
       "day": "Fri",
       "high": "55",
       "low": "50",
       "text": "Scattered Showers"
      },
      {
       "code": "12",
       "date": "15 Sep 2018",
       "day": "Sat",
       "high": "55",
       "low": "50",
       "text": "Rain"
      },
      {
       "code": "23",
       "date": "16 Sep 2018",
       "day": "Sun",
       "high": "52",
       "low": "50",
       "text": "Breezy"
      },
      {
       "code": "26",
       "date": "17 Sep 2018",
       "day": "Mon",
       "high": "51",
       "low": "50",
       "text": "Cloudy"
      }
     ],
     "description": "<![CDATA[<img src=\"http://l.yimg.com/a/i/us/we/52/28.gif\"/>\n<BR />\n<b>Current Conditions:</b>\n<BR />Mostly Cloudy\n<BR />\n<BR />\n<b>Forecast:</b>\n<BR /> Sat - Partly Cloudy. High: 51Low: 43\n<BR /> Sun - Mostly Cloudy. High: 54Low: 46\n<BR /> Mon - Partly Cloudy. High: 55Low: 46\n<BR /> Tue - Cloudy. High: 53Low: 50\n<BR /> Wed - Mostly Cloudy. High: 51Low: 47\n<BR />\n<BR />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/\">Full Forecast at Yahoo! Weather</a>\n<BR />\n<BR />\n<BR />\n]]>",
     "guid": {
      "isPermaLink": "false"
     }
    }
   }
  }
 }
}
'''
