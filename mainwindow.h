#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QtNetwork/QNetworkReply"
#include "QtNetwork/QNetworkRequest"
#include "QProcess"
#include "QThread"
#include "QMouseEvent"
#include "QtSvg/QSvgWidget"
#include "QLabel"
#include "QMenu"
#include "QTimer"
#include "manager.h"
#include "QCloseEvent"

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT
signals:
    void begin();
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void get_data(void);
    void show_data(void);

private slots:
    void change(std::string,int);
    void refresh(void);
    void setting(void);
    void exitApp(void);
private:
    void set_config(void);
    QString city;
    int time;
    QTimer* timer;
    bool flag;
    class Worker:public QThread
    {
    private:
        QString city;
    public:
        Worker(QString new_city):QThread(),city(new_city){}
        void run(void) override
        {
            QProcess p;
            connect(&p,SIGNAL(finished(int)),this,SLOT(quit()));
            QString command="python /tmp/weather_tmp.py "+city;
            //qDebug()<<command;
            p.execute(command);

        }
    };
    /*
   class Manager_thread:public QThread
    {
    public:
        void run(void) override
        {
            manager* config_manager=new manager();
            config_manager->show();
            config_manager->exec();

        }
    };
   */
    virtual void enterEvent(QEvent *);
    virtual void leaveEvent(QEvent *);
    void set_image(QLabel*,QString);
    void set_output(int,QStringList);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    bool m_Drag; //记录鼠标是否按下
    QPoint m_DragPosition;//记录鼠标位置
    QPixmap svg2png(QString);
    QPixmap svg2png_big(QString);
    void contextMenuEvent(QContextMenuEvent *);

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
