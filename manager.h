#ifndef MANAGER_H
#define MANAGER_H

#include <QDialog>
#include "QCloseEvent"
namespace Ui {
class manager;
}

class manager : public QDialog
{
    Q_OBJECT
signals:
    void ExitWin();
    void refresh(std::string,int);
public:
    explicit manager(QWidget *parent = nullptr);
    ~manager();

private slots:
    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

private:
    Ui::manager *ui;
    QStringList strings;
    virtual void closeEvent(QCloseEvent *e);
};

#endif // MANAGER_H
