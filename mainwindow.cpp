#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QProcess"
#include "QString"
#include "iostream"
#include "QFile"
#include "QThread"
#include "QDebug"
#include "QGraphicsOpacityEffect"
#include "QImage"
#include "QSvgRenderer"
#include "QPainter"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnBottomHint|Qt::Tool);
    this->setWindowOpacity(0.4);
    //this->resize(1000,1000);
    setMouseTracking(true);
    this->setStyleSheet("MainWindow {border-image: url(:/a.jpg)}");
    city="";
    time=5;
    QFile file(":/test.py");
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug()<<"file not opened"<<endl;
    }
    qDebug()<<"file opened"<<endl;
    QString s=file.readAll();
    file.close();
    QFile out("/tmp/weather_tmp.py");
    if(!out.open(QIODevice::WriteOnly)) {
        qDebug()<<"filenot opened"<<endl;
    }
    out.write(s.toStdString().c_str());
    out.close();
    set_config();
    //get_data();
    //show_data();
}

void MainWindow::set_config()
{
    QTimer::singleShot(100,this,SLOT(refresh()));
    QFile file("config.ini");
    if(file.exists()==false)
    {
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
        timer->start(1000*5*60);
    }
    else
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        using namespace rapidjson;
        QTextStream in(&file);
        int i=0;
        while (!in.atEnd()) {
            QString line = in.readLine();
            qDebug()<<line;
            Document d;
            d.Parse(line.toStdString().c_str());
            city=QString(d["City"].GetString());
            time=d["Time"].GetInt();
            //qDebug()<<city<<time;
        }
        file.close();
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
        timer->start(1000*5*60);

    }

}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setting(void)
{
    /*
    Manager_thread* p=new Manager_thread();
    p->run();
    p->quit();
    refresh();
*/
    manager* config_manager=new manager(this);
    connect(config_manager,SIGNAL(refresh(std::string,int)),this,SLOT(change(std::string,int)));
    //p->moveToThread(&workerThread);
    //connect(this, &MainWindow::begin, manager, &manager::manager);
    //workerThread.start();
    //connect(config_manager,SIGNAL(ExitWin()),this,SLOT(show()));
    //this->hide();
    config_manager->setModal(false);
    config_manager->show();

    //config_manager->exec();
    //this->show();
}

void MainWindow::change(std::string new_city,int new_time)
{
    timer->stop();
    this->city=QString(new_city.c_str());
    this->time=new_time;
    timer->start(1000*time*60);
    refresh();
}

void MainWindow::refresh(void)
{
    get_data();
    if(flag==true)
        show_data();
    else
    {
        ui->config->setText("Error:No such city!");
        flag=true;
    }
}

void MainWindow::contextMenuEvent(QContextMenuEvent *)
{
    QMenu *menu = new QMenu(this);
    QAction* config_action=menu->addAction(QString("设置"));
    QAction* exit_action=menu->addAction(QString("退出"));
    connect(exit_action,SIGNAL(triggered()),this,SLOT(exitApp()));
    connect(config_action,SIGNAL(triggered()),this,SLOT(setting()));
    menu->exec(QCursor::pos());
}

void MainWindow::exitApp()
{
    QApplication::quit();
}

void MainWindow::get_data(void)
{
    Worker w(city);
    w.start();
    w.wait();
}

void MainWindow::show_data(void)
{
    QFile file("tmp.db");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QTextStream in(&file);
    int i=0;
    flag=true;
    while (!in.atEnd()) {
        QString line = in.readLine();
        if(line=="DEADBEEF")
        {
            flag=false;
            break;
        }
        QStringList tmp=line.split(',');
        //qDebug()<<tmp;
        if (i<=9)
            set_output(i,tmp);
        else if(i==10)
        {
            ui->cur_temp->setText(tmp[0]+"°C");
            //ui->label_6->setPixmap(svg2png_big(QString(":/wi-celsius.svg")));
            ui->label->setText(tmp[1]+tmp[2]);
            ui->label_6->setText(tmp[4]+'\n'+tmp[3]);
        }
        else if(i==11)
        {
            if(line=="BEEFBEEF")
            {
                ui->pm25->setText("");
            }
            else
            {
                ui->pm25->setText("PM2.5: "+tmp[0]);
                ui->pm25_2->setText("PM10: "+tmp[1]);
                ui->pm25_3->setText("CO: "+tmp[2]);
                ui->pm25_4->setText("空气质量指数: "+tmp[3]);
            }
        }
        i++;
    }
    file.close();
    QFile::remove("tmp.db");
}

void MainWindow::set_output(int index,QStringList tmp)
{
    switch (index) {
    case 0:
        ui->time_1->setText(tmp[0]);
        ui->degree_1->setText(tmp[2]+"~"+tmp[1]+"°C");
        set_image(ui->label_1,tmp[3]);
        break;
    case 1:
        ui->time_2->setText(tmp[0]);
        ui->degree_2->setText(tmp[2]+"~"+tmp[1]+"°C");
        set_image(ui->label_2,tmp[3]);
        break;
    case 2:
        ui->time_3->setText(tmp[0]);
        ui->degree_3->setText(tmp[2]+"~"+tmp[1]+"°C");
        set_image(ui->label_3,tmp[3]);
        break;
    case 3:
        ui->time_4->setText(tmp[0]);
        ui->degree_4->setText(tmp[2]+"~"+tmp[1]+"°C");
        set_image(ui->label_4,tmp[3]);
        break;
    case 4:
        ui->time_5->setText(tmp[0]);
        ui->degree_5->setText(tmp[2]+"~"+tmp[1]+"°C");
        set_image(ui->label_5,tmp[3]);
        break;
    default:
        break;
    }
}

QPixmap MainWindow::svg2png(QString file)
{
    QSvgRenderer renderer(file);
    QPixmap image(75, 75);
    image.fill( Qt::transparent );
    QPainter painter(&image);
    renderer.render(&painter);
    return image;
}

QPixmap MainWindow::svg2png_big(QString file)
{
    QSvgRenderer renderer(file);
    QPixmap image(175, 175);
    image.fill( Qt::transparent );
    QPainter painter(&image);
    renderer.render(&painter);
    return image;
}

void MainWindow::set_image(QLabel* label,QString index)
{
    int i=index.toInt();
    switch (i) {
    case 0:
        label->setPixmap(svg2png(QString(":/wi-tornado.svg")));
        break;
    case 1:
    case 2:
        label->setPixmap(svg2png(QString(":/wi-hurricane.svg")));
        break;
    case 3:
    case 4:
    case 37:
    case 38:
    case 39:


        label->setPixmap(svg2png(QString(":/wi-thunderstorm.svg")));
        break;
    case 5:
    case 6:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 35:
    case 47:
    case 40:
    case 45:
        label->setPixmap(svg2png(QString(":/wi-rain.svg")));
        break;
    case 7:
    case 13:
    case 16:
    case 15:
    case 14:
    case 41:
    case 42:
    case 43:
    case 46:
        label->setPixmap(svg2png(QString(":/wi-snow.svg")));
        break;
    case 17:
        label->setPixmap(svg2png(QString(":/wi-hail.svg")));
        break;
    case 18:
        label->setPixmap(svg2png(QString(":/wi-sleet.svg")));
        break;
    case 19:
        label->setPixmap(svg2png(QString(":/wi-dust.svg")));
        break;
    case 20:
        label->setPixmap(svg2png(QString(":/wi-fog.svg")));
        break;
    case 21:
        label->setPixmap(svg2png(QString(":/wi-haze.svg")));
        break;
    case 22:
        label->setPixmap(svg2png(QString(":/wi-smoke.svg")));
        break;
    case 23:
    case 24:
        label->setPixmap(svg2png(QString(":/wi-windy.svg")));
        break;
    case 25:
        label->setPixmap(svg2png(QString(":/wi-snowflake-cold.svg")));
        break;
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
        label->setPixmap(svg2png(QString(":/wi-cloudy.svg")));
        break;
    case 31:
    case 33:
        label->setPixmap(svg2png(QString(":/wi-night-clear.svg")));
        break;
    case 32:
    case 34:
        label->setPixmap(svg2png(QString(":/wi-day-sunny.svg")));
        break;
    case 36:
        label->setPixmap(svg2png(QString(":/wi-hot.svg")));
        break;

    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_Drag = true;
        m_DragPosition = event->globalPos() - this->pos();
        event->accept();
    }
}

void MainWindow::enterEvent(QEvent *)
{
    this->setWindowOpacity(0.8);
}

void MainWindow::leaveEvent(QEvent *)
{
    this->setWindowOpacity(0.4);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (m_Drag && (event->buttons() && Qt::LeftButton))
    {
        move(event->globalPos() - m_DragPosition);
        event->accept();
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *)
{
    m_Drag=false;
}
